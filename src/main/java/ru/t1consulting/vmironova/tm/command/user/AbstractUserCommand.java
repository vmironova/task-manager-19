package ru.t1consulting.vmironova.tm.command.user;

import ru.t1consulting.vmironova.tm.api.service.IAuthService;
import ru.t1consulting.vmironova.tm.api.service.IUserService;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;
import ru.t1consulting.vmironova.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    public void showUser(User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
