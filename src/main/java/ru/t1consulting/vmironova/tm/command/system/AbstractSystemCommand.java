package ru.t1consulting.vmironova.tm.command.system;

import ru.t1consulting.vmironova.tm.api.service.ICommandService;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
