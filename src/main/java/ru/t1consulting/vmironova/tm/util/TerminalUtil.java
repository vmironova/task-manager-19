package ru.t1consulting.vmironova.tm.util;

import ru.t1consulting.vmironova.tm.exception.AbstractException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        try {
            final String value = nextLine();
            return Integer.parseInt(value);
        } catch (final RuntimeException e) {
            throw new NumberInvalidException();
        }
    }

    final class NumberInvalidException extends AbstractException {
        public NumberInvalidException() {
            super("Error! Value is not a number.");
        }
    }

}
