package ru.t1consulting.vmironova.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
