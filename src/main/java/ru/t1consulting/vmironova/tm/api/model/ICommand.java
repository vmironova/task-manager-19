package ru.t1consulting.vmironova.tm.api.model;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute();

}
