package ru.t1consulting.vmironova.tm.api.service;

import ru.t1consulting.vmironova.tm.api.repository.IRepository;
import ru.t1consulting.vmironova.tm.enumerated.Sort;
import ru.t1consulting.vmironova.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);

}
